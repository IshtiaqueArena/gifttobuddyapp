package com.arena.gifttobuddy.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arena.gifttobuddy.Models.AuthenticationListener;
import com.arena.gifttobuddy.R;
import com.arena.gifttobuddy.Utils.SharedPreference;
import com.arena.gifttobuddy.retrofit.Url;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.tanvir.test_library.BasicFunction;
import com.tanvir.test_library.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class Login extends AppCompatActivity implements View.OnClickListener,BasicFunctionListener {

    Button signinBtn;
    EditText emailEdt,passwordEdt;
    String email,password;
    BasicFunction bf;
    String name,gender,birthday;
    GoogleSignInClient mGoogleSignInClient;
    GoogleSignInOptions gso;
    GoogleSignInAccount account;
    private LoginButton fbLoginBtn;
    private SignInButton googleLoginBtn;
    private CallbackManager callbackManager;
    private ImageView fb,google,instalogin;
    AccessToken accessToken;
    SharedPreference sharedPreference;
    private AuthenticationListener mListener;
    String user_id;
    String loggedin;
    TextView register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bf = new BasicFunction(this, this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        sharedPreference = SharedPreference.getInstance(Login.this);
        loggedin = sharedPreference.getDataLogin("loggedin");
        if(!loggedin.equalsIgnoreCase("0")){
            Intent intent = new Intent(Login.this,HomeActivity.class);
            startActivity(intent);
            finish();
        }
        setContentView(R.layout.activity_login);

        sharedPreference = SharedPreference.getInstance(Login.this);
        user_id = sharedPreference.getData("user_id");

        register = findViewById(R.id.login_tv);
        signinBtn = findViewById(R.id.signinBtn);
        emailEdt = findViewById(R.id.emailEdt);
        passwordEdt = findViewById(R.id.passwordEdt);

        instalogin = findViewById(R.id.insta_login);
        instalogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initializeWebView();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this,Register.class);
                startActivity(intent);
                finish();
            }
        });
        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate(emailEdt) && validate(passwordEdt)){
                    login();
                }

            }
        });

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(Login.this, gso);

        account = GoogleSignIn.getLastSignedInAccount(Login.this);
//        if (account != null)
//            updateUI(account);
//        else
//            btnLogout.setVisibility(View.GONE);

        FacebookSdk.sdkInitialize(getApplicationContext());
        accessToken = AccessToken.getCurrentAccessToken();
        Log.e("accesstoken", String.valueOf(accessToken));

        callbackManager = CallbackManager.Factory.create();
        fb = (ImageView) findViewById(R.id.fb_login);
        google = (ImageView) findViewById(R.id.google_login);

        fbLoginBtn = (LoginButton) findViewById(R.id.login_button);
        googleLoginBtn = (SignInButton) findViewById(R.id.google_button);

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        fb.setOnClickListener(this);

        mListener = new AuthenticationListener() {
            @Override
            public void onSuccess(String accessToken) {
                Log.e("accesstoken",accessToken);
            }

            @Override
            public void onFail(String error) {

            }
        };

        fbLoginBtn.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));

        fbLoginBtn.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        System.out.println("onSuccess");

                        String accessToken = loginResult.getAccessToken()
                                .getToken();
                        Log.i("accessToken", accessToken);

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object,
                                                            GraphResponse response) {

                                        Log.i("LoginActivity",
                                                response.toString());
                                        try {
                                            name = object.getString("name");
                                            email = object.getString("email");
                                            gender = object.getString("gender");
                                            birthday = object.getString("birthday");

                                            try {
                                                JSONObject jsonObject = new JSONObject();
                                                jsonObject.put("name",name);
                                                jsonObject.put("email", email);


                                                bf.getResponceData(Url.registration,jsonObject.toString(),116,"POST");
                                                Intent intent = new Intent(Login.this,HomeActivity.class);
                                                startActivity(intent);



                                            } catch (JSONException e) {
                                                Log.e("MYAPP", "unexpected JSON exception"+ e.getMessage());
                                            }

                                            Log.e("name",name+"email"+email+"gender"+gender+"birthday"+birthday);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields",
                                "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        System.out.println("onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        System.out.println("onError");
                        Log.v("LoginActivity", exception.getCause().toString());
                    }
                });


    }

    private void initializeWebView() {
        Intent intent = new Intent(Login.this,InstaLogin.class);
        startActivity(intent);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 1);
    }

    private boolean validate(EditText editText) {
        if (editText.getText().toString().trim().length() > 0) {
            return true; // returs true if field is not empty
        }
        editText.setError("Please Fill This");
        editText.requestFocus();
        return false;
    }

    private void login() {
        email = emailEdt.getText().toString();
        password = passwordEdt.getText().toString();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", email);
            jsonObject.put("password", password);

            bf.getResponceData(Url.login,jsonObject.toString(),116,"GET");


        } catch (JSONException e) {
            Log.e("MYAPP", "unexpected JSON exception"+ e.getMessage());
        }
    }
//
//    private void showPop() {
//        AlertDialog.Builder builder = newbg AlertDialog.Builder(Login.this);
//        View view = getLayoutInflater().inflate(R.layout.request_sent_confirmation, null);
//        builder.setView(view);
//        final AlertDialog dialog = builder.create();
//        dialog.requestWindowFeature(DialogFragment.STYLE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        dialog.getWindow().setLayout(100,100);
//        //dialog.getWindow().getAttributes().windowAnimations = R.style.popup_window_animation_phone;
//        dialog.show();
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(Login.this, Register.class));
            finish();

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void OnServerResponce(String jsonObject, int RequestCode) {
        Log.e("jsonobject",jsonObject.toString());
        if(RequestCode == 116){
            JSONObject jsonobject = null;
            try {
                jsonobject = new JSONObject(jsonObject);
                JSONObject response = jsonobject.getJSONObject("response");
                int user_id = response.getInt("user_id");
                sharedPreference.saveData("user_id", String.valueOf(user_id));
                int status = response.getInt("status");
                String message = response.getString("message");
                if(status==1){
                    sharedPreference.saveDataLogin("loggedin", "1");
                    Toast.makeText(this, ""+message, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Login.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(this, ""+message, Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void OnConnetivityError() {

    }

    @Override
    public void onClick(View v) {
        if (v == fb) {
            fbLoginBtn.performClick();
        }
    }
}
