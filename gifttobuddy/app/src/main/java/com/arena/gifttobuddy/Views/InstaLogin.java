package com.arena.gifttobuddy.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.arena.gifttobuddy.Models.AuthenticationListener;
import com.arena.gifttobuddy.R;

public class InstaLogin extends AppCompatActivity {

    WebView webView;
    AuthenticationListener listener;
    private final String url = "https://api.instagram.com/" + "oauth/authorize/?app_id=" +
            "1059420324433709"
            + "&redirect_uri="
            + "https://socialsizzle.heroku.com/auth/"
            + "&response_type=code"
            + "&scope=user_profile,user_media";

    public AuthenticationListener getListener() {
        return listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insta_login);

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        //Log.d(TAG, "url: " + url);
        webView.setWebViewClient(new WebViewClient() {

            String access_token;
            boolean authComplete;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                //Log.d(TAG, "onPageStarted called");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //Log.d(TAG, "onPageFinished called " + url);
                if (url.contains("?code=") && !authComplete) {
                    // Log.d(TAG, " inside access_token");
                    access_token = url;
                    //get the whole token after "=" sign
                    access_token = access_token.replace("https://www.instagram.com/?code=","");
                    access_token = access_token.replace("#_","");
                    Log.d("Token", "token: " + access_token);
                    authComplete = true;

                    webView.loadUrl("https://instagram.com/accounts/logout/");

                    //dismiss();
                } else if (url.contains("?error")) {
                    // Log.d(TAG, "getting error fetching access token");
                    //dismiss();
                } else {
                    // Log.d(TAG, "outside both" + url.toString());
                }
            }
        });


    }
}
