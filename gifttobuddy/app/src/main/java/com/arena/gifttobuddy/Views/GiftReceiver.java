package com.arena.gifttobuddy.Views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.widget.NestedScrollView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.arena.gifttobuddy.Helpers.RoundedImg;
import com.arena.gifttobuddy.R;

public class GiftReceiver extends AppCompatActivity {

    ImageView profileImg;
    RoundedImg roundedImage;
    CardView cardView;
    ScrollView scrollview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_gift_receiver);

        profileImg = findViewById(R.id.imageview_account_profile);
        //cardView = findViewById(R.id.cardView);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ibrahim);
        roundedImage = new RoundedImg(bm);
        profileImg.setImageDrawable(roundedImage);

    }
}
